/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package excepciones;

import excepciones.util.Calculos;
import excepciones.util.ManejoArchivos;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author jesus
 */
public class Excepciones {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        calculos();
    }

    public static void archivo() {
        try {
            ManejoArchivos.crearArchivo("D:\\curso\\prueba.txt");
            ManejoArchivos.escribirEnArchivos("D:\\curso\\prueba.txt", "Nuevo archivo con contenido");
        } catch (IOException ex) {
            System.out.println("Error : " + ex);
        } finally {
            System.out.println("Termino");
        }
    }

    public static void calculos() {
        double suma = 0;
        double resta = 0;
        double division = 0;
        double divisionNreal = 0;
        double primeroNumero;
        double segundoNumero;

        try {
            Scanner in = new Scanner(System.in);
            System.out.println("Introduce un número:");
            primeroNumero = Double.valueOf(in.nextLine());
            System.out.println("Introduce otro número:");
            segundoNumero = Double.valueOf(in.nextLine());

            suma = Calculos.sumar(primeroNumero, segundoNumero);
            resta = Calculos.restar(primeroNumero, segundoNumero);
            divisionNreal = Calculos.divisionNReales(primeroNumero, segundoNumero);
            division = Calculos.division((int) primeroNumero, (int) segundoNumero);
            Calculos.resultadoNegativo(resta);

        } catch (NumberFormatException e) {
            System.out.println("Error de formato: " + e);
        } catch (ArithmeticException e) {
            System.out.println("Error aritmético: " + e);
        } catch (IllegalStateException e) {
            System.out.println("Error IllegalStateException: " + e);
        } finally {
            System.out.println("Suma es:" + suma);
            System.out.println("La resta es:" + resta);
            System.out.println("La division es: " + division);
            System.out.println("La division número real es: " + divisionNreal);
        }
    }

}
