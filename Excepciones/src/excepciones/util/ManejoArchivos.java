/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package excepciones.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author jesus
 */
public class ManejoArchivos {

    public static void crearArchivo(String ruta) throws IOException {
        File file = new File(ruta);
        // Si el archivo no existe es creado
        if (!file.exists()) {
            file.createNewFile();
        }
    }

    public static void escribirEnArchivos(String ruta, String contenido) {
        FileWriter fw = null;
        try {
            File file = new File(ruta);
            fw = new FileWriter(file);
            try (BufferedWriter bw = new BufferedWriter(fw)) {
                bw.write(contenido);
            }
        } catch (IOException ex) {
            System.out.println("Error : " + ex);
        } finally {
            try {
                fw.close();
            } catch (IOException ex) {
                System.out.println("Error : " + ex);
            }
        }
    }

}
